//
//  Events.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 30/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import UIKit

class Events: BaseViewController {
    
    var user = EUsers()
    var date = Date()
    
    var segmentedController = UISegmentedControl()
    var segmentIndexSelected = Int()
    
    var graphView = UIView ()
    var eventDesc = UIView ()
    var eventDescData = EEvents()
    
    var listView = UIView ()
    var eventsTable = UITableView()
    
    var padding = CGFloat()
    var addtionalY = CGFloat()
    
    var btnPoints = [UIButton]()
    
    var filterType = filterEvent.date

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.padding = self.view.frame.width / 20
        self.addtionalY = self.navigationController!.navigationBar.frame.maxY + UIApplication.shared.statusBarFrame.height
        let tabBar = self.tabBarController as! TabBarController
        self.user = tabBar.user
        
        self.setupNavBar()
        self.setupGraphViewPage(filter: filterType)
        self.setupListViewPage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.eventsTable.reloadData()
        self.graphView.subviews.forEach({ $0.removeFromSuperview() })
        self.graphView.removeFromSuperview()
        self.setupGraphViewPage(filter: filterType)
        self.changeViewPage(index: self.segmentedController.selectedSegmentIndex)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Setup Navigation Bar
extension Events {
    func setupNavBar () {
        // Add button
        let addBtn: UIBarButtonItem = {
            let barBtn = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addBtnAction))
            return barBtn
        }()
        self.navigationItem.leftBarButtonItem = addBtn
        
        // Filter button
        let filterBtn: UIBarButtonItem = {
            let barBtn = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(filterBtnAction))
            return barBtn
        }()
        self.navigationItem.rightBarButtonItem = filterBtn
        
        // Segmented control
        self.segmentedController = UISegmentedControl(items: ["Graph", "List"])
        self.segmentedController.frame.size.width = self.view.frame.width / 2
        self.segmentedController.selectedSegmentIndex = 0
        self.segmentedController.addTarget(self, action: #selector(segmentedControllerAction(_:)), for: .valueChanged)
        self.navigationItem.titleView = self.segmentedController
        
        self.changeViewPage(index: self.segmentedController.selectedSegmentIndex)
        self.addtionalY = (self.navigationController?.navigationBar.frame.height)! + UIApplication.shared.statusBarFrame.height
    }
    
    func addBtnAction () {
        let vc = AddEvent()
        vc.user = self.user
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func filterBtnAction () {
        let alert = UIAlertController(title: "Filter Events", message: "Filter events by:", preferredStyle: .alert)
        let filterNameAction = UIAlertAction(title: "Name", style: .default, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
                self.graphView.subviews.forEach({ $0.removeFromSuperview() })
                self.listView.subviews.forEach({ $0.removeFromSuperview() })
                self.filterType = .name
                self.setupGraphViewPage(filter: .name)
                self.setupListViewPage()
                self.eventsTable.reloadData()
                self.changeViewPage(index: self.segmentIndexSelected)
            }})
        alert.addAction(filterNameAction)
        let filterDateAction = UIAlertAction(title: "Date", style: .default, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            self.graphView.subviews.forEach({ $0.removeFromSuperview() })
            self.listView.subviews.forEach({ $0.removeFromSuperview() })
                self.filterType = .date
                self.setupGraphViewPage(filter: .date)
                self.setupListViewPage()
                self.eventsTable.reloadData()
                self.changeViewPage(index: self.segmentIndexSelected)
            }})
        alert.addAction(filterDateAction)
        let filterCostAction = UIAlertAction(title: "Cost", style: .default, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
                self.graphView.subviews.forEach({ $0.removeFromSuperview() })
                self.filterType = .cost
                self.setupGraphViewPage(filter: .cost)
                self.setupListViewPage()
                self.eventsTable.reloadData()
                self.changeViewPage(index: self.segmentIndexSelected)
            }})
        alert.addAction(filterCostAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            
            }})
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func segmentedControllerAction (_ sender: UISegmentedControl) {
        self.segmentIndexSelected = sender.selectedSegmentIndex
        self.changeViewPage(index: self.segmentIndexSelected)
    }
    
    func changeViewPage (index: Int) {
        if index == 0 {
            //self.listView.removeFromSuperview()
            //self.setupGraphViewPage()
            self.listView.isHidden = true
            self.graphView.isHidden = false
        } else {
            //self.graphView.removeFromSuperview()
            //self.setupListViewPage()
            self.listView.isHidden = false
            self.graphView.isHidden = true
        }
    }
}

// MARK: - Graph View
extension Events {
    func setupGraphViewPage (filter: filterEvent) {
        self.graphView = {
            let view = UIView(frame: CGRect(x: 0, y: addtionalY, width: self.view.frame.width, height: self.view.frame.height / 4))
            
            return view
        } ()
        self.graphView.isHidden = false
        self.view.addSubview(self.graphView)
        
        if self.user.events.count > 0 {
            let graph = EventsGraphView()
            graph.user = self.user
            graph.setupData(filter: filter)
            graph.frame.origin = CGPoint(x: 0, y: 0)
            self.btnPoints.append(contentsOf: graph.btnPoints)
            self.btnPoints.forEach({ $0.addTarget(self, action: #selector(btnPointAction(_:)), for: .touchUpInside) })
            
            let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: graph.frame.height + addtionalY))
            scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, (addtionalY / 1.3) * -1, 0)
            scrollView.contentSize.width = graph.frame.width * 1.2
            scrollView.addSubview(graph)
            
            self.graphView.frame.size.height = scrollView.frame.size.height + addtionalY
            self.graphView.addSubview(scrollView)
        } else {
            let noEvent: UILabel = {
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.graphView.frame.width, height: self.graphView.frame.width / 2))
                label.textColor = UIColor.lightGray
                label.textAlignment = .center
                label.font = UIFont.systemFont(ofSize: label.frame.height / 15, weight: UIFontWeightBold)
                label.numberOfLines = 0
                label.text = "No event added. Add event by clicking add on top left."
                
                return label
            } ()
            self.graphView.addSubview(noEvent)
        }
    }
    
    func btnPointAction (_ sender: UIButton) {
        guard let btnPoint = sender as? UIButton else { return }
        
        self.btnPoints.forEach {
            btn in
            if btn == btnPoint {
                btnPoint.setBackgroundColor(UIColor(rgb: 0x1774D7), forState: .normal)
            } else {
                btn.setBackgroundColor(UIColor.lightGray, forState: .normal)
            }
        }
        
        var events = [EEvents]()
        let res = self.user.events.sorted(byKeyPath: self.filterType.rawValue, ascending: true)
        res.forEach { e in events.append(e)}
        
        let event = events[btnPoint.tag]
        self.eventDescData = event
        self.eventDescription(event: event)
    }
    
    func eventDescription (event: EEvents) {
        self.eventDesc.subviews.forEach({ $0.removeFromSuperview() })
        self.eventDesc.removeFromSuperview()
        
        self.eventDesc.frame = CGRect(x: 0, y: self.graphView.frame.maxY - (addtionalY * 2), width: self.view.frame.width, height: UIScreen.main.bounds.height / 8)
        
        let cellHeight = UIScreen.main.bounds.height / 8
        let cellWidth = UIScreen.main.bounds.width
        
        // Cost
        let cost: UILabel = {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: cellWidth / 5, height: cellHeight))
            label.backgroundColor = UIColor(rgb: 0x1774D7)
            label.textColor = .white
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: label.frame.height / 5, weight: UIFontWeightBold)
            label.text = "$\(event.cost)K"
            
            return label
        } ()
        self.eventDesc.addSubview(cost)
        
        // Name
        let name: UILabel = {
            let label = UILabel(frame: CGRect(x: cost.frame.maxX + padding, y: padding / 2, width: cellWidth - (cost.frame.maxX + padding), height: cellHeight / 4))
            label.textColor = .gray
            label.font = UIFont.systemFont(ofSize: label.frame.height * 0.80, weight: UIFontWeightMedium)
            label.text = event.name
            
            return label
        } ()
        self.eventDesc.addSubview(name)
        
        // Date
        let date: UILabel = {
            let label = UILabel(frame: CGRect(x: name.frame.origin.x, y: name.frame.maxY + (padding / 4), width: cellWidth - (cost.frame.maxX + padding), height: cellHeight / 4))
            label.textColor = .gray
            label.font = UIFont.systemFont(ofSize: label.frame.height * 0.70, weight: UIFontWeightRegular)
            label.text = event.date.getMonthAndDayName()
            
            return label
        } ()
        self.eventDesc.addSubview(date)
        
        // Address
        let address: UILabel = {
            let label = UILabel(frame: CGRect(x: name.frame.origin.x, y: date.frame.maxY, width: cellWidth - (cost.frame.maxX + padding), height: cellHeight / 4))
            label.textColor = .gray
            label.font = UIFont.systemFont(ofSize: label.frame.height * 0.70, weight: UIFontWeightRegular)
            label.text = event.address
            
            return label
        } ()
        self.eventDesc.addSubview(address)
        
        // Invisible btn to description of event
        let btnToDesc: UIButton = {
            let btn = UIButton(frame: CGRect(x: 0, y: 0, width: self.eventDesc.frame.width, height: self.eventDesc.frame.height))
            btn.addTarget(self, action: #selector(eventDescBtnAction), for: .touchUpInside)
            
            return btn
        }()
        self.eventDesc.addSubview(btnToDesc)
        
        // Separator
        let separator = UIView(frame: CGRect(x: cost.frame.maxX, y: cellHeight - 1.0, width: cellWidth - cost.frame.maxX, height: 1.0))
        separator.backgroundColor = UIColor.lightGray
        self.eventDesc.addSubview(separator)
        
        self.graphView.addSubview(eventDesc)
    }
    
    func eventDescBtnAction () {
        print(eventDescData)
        let vc = EditEvent()
        vc.user = self.user
        vc.event = self.eventDescData
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - List View
extension Events: UITableViewDelegate, UITableViewDataSource {
    func setupListViewPage () {
        //setup tableView
        self.eventsTable.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.eventsTable.register(EventsCell.self, forCellReuseIdentifier: "cell")
        self.eventsTable.delegate = self
        self.eventsTable.dataSource = self
        self.eventsTable.tableFooterView = UIView(frame: .zero)
        self.eventsTable.separatorStyle = .none
        
        self.listView = {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            view.addSubview(self.eventsTable)
            return view
        } ()
        self.listView.isHidden = true
        self.view.addSubview(self.listView)
    }
    
    // TableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let events = self.user.events
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.eventsTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EventsCell
        cell.accessoryType = .disclosureIndicator
        
        //let event = self.user.events[(indexPath as IndexPath).item]
        
        var events = [EEvents]()
        let res = self.user.events.sorted(byKeyPath: self.filterType.rawValue, ascending: true)
        res.forEach { e in events.append(e)}
        
        let event = events[(indexPath as IndexPath).item]
        
        let date = event.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        
        let result = dateFormatter.string(from: date)
        
        cell.cost.text = "$\(event.cost)K"
        cell.name.text = event.name
        cell.date.text = result
        cell.address.text = event.address
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var events = [EEvents]()
        let res = self.user.events.sorted(byKeyPath: self.filterType.rawValue, ascending: true)
        res.forEach { e in events.append(e)}
        
        let event = events[(indexPath as IndexPath).item]
        
        let vc = EditEvent()
        vc.user = self.user
        vc.event = event
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height / 8
    }
}

// MARK: - Event Cell class
class EventsCell: UITableViewCell {
    var name = UILabel()
    var cost = UILabel()
    var date = UILabel()
    var address = UILabel()
    var contactNo = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let padding = self.frame.width / 20
        let cellHeight = UIScreen.main.bounds.height / 8
        let cellWidth = UIScreen.main.bounds.width
        
        // Cost
        self.cost = {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: cellWidth / 5, height: cellHeight))
            label.backgroundColor = UIColor(rgb: 0x1774D7)
            label.textColor = .white
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: label.frame.height / 5, weight: UIFontWeightBold)
            
            return label
        } ()
        self.contentView.addSubview(self.cost)
        
        // Name
        self.name = {
            let label = UILabel(frame: CGRect(x: self.cost.frame.maxX + padding, y: padding / 2, width: cellWidth - (self.cost.frame.maxX + padding), height: cellHeight / 4))
            label.textColor = .gray
            label.font = UIFont.systemFont(ofSize: label.frame.height * 0.80, weight: UIFontWeightMedium)
            
            return label
        } ()
        self.contentView.addSubview(self.name)
        
        // Date
        self.date = {
            let label = UILabel(frame: CGRect(x: self.name.frame.origin.x, y: self.name.frame.maxY + (padding / 4), width: cellWidth - (self.cost.frame.maxX + padding), height: cellHeight / 4))
            label.textColor = .gray
            label.font = UIFont.systemFont(ofSize: label.frame.height * 0.70, weight: UIFontWeightRegular)
            
            return label
        } ()
        self.contentView.addSubview(self.date)
        
        // Address
        self.address = {
            let label = UILabel(frame: CGRect(x: self.name.frame.origin.x, y: self.date.frame.maxY, width: cellWidth - (self.cost.frame.maxX + padding), height: cellHeight / 4))
            label.textColor = .gray
            label.font = UIFont.systemFont(ofSize: label.frame.height * 0.70, weight: UIFontWeightRegular)
            
            return label
        } ()
        self.contentView.addSubview(self.address)
        
        // Separator
        let separator = UIView(frame: CGRect(x: self.cost.frame.maxX, y: cellHeight - 1.0, width: cellWidth - self.cost.frame.maxX, height: 1.0))
        separator.backgroundColor = UIColor.lightGray
        self.contentView.addSubview(separator)
        
        let separatorBtn = UIView(frame: CGRect(x: 0, y: cellHeight - 1.0, width: self.cost.frame.maxX, height: 1.0))
        separatorBtn.backgroundColor = UIColor.white
        self.contentView.addSubview(separatorBtn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

enum filterEvent: String {
    case name = "name"
    case date = "date"
    case cost = "cost"
}

//
//  Login.swift
//  EventsMgnt
//
//  Created by Louie Je Decatoria on 24/08/2017.
//
//

import UIKit
import RealmSwift

class Login: BaseViewController {
    
    // Elements
    var loginView = UIView()
    var usernameLoginTF = UITextField()
    var passwordLoginTF = UITextField()
    
    var signUpView = UIView()
    var usernameSignupTF = UITextField()
    var emailSignupTF = UITextField()
    var passwordSignupTF = UITextField()
    var rePasswordSignupTF = UITextField()
    
    // Load data
    var users: Results<EUsers>!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getData()
        
        self.setupBackground()
        self.setupLoginView()
        self.setupSignUpView()
    }
    
    func getData () {
        self.users = uiRealm.objects(EUsers.self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

// MARK: - Setup views
extension Login {
    func setupBackground () {
        let padding = self.view.frame.width / 20
        
        //Setup view for bg
        let bgView: UIView = {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 3))
            let imgView = UIImageView(frame: CGRect(x: 0, y: padding, width: view.frame.width, height: view.frame.height - (padding * 2)))
            imgView.image = #imageLiteral(resourceName: "EventsBanner")
            view.addSubview(imgView)
            
            return view
        }()
        self.view.addSubview(bgView)
        
        //Setup any img on bg
        
        // setup title
        let appTitle: UILabel = {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: bgView.frame.width, height: bgView.frame.height))
            label.text = "EVENTY"
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: label.frame.height / 4, weight: UIFontWeightMedium)
            label.textColor = .white
            
            return label
        }()
        bgView.addSubview(appTitle)
    }
}

// MARK: - Login View
extension Login {
    func setupLoginView () {
        let padding = self.view.frame.width / 20
        let width = self.view.frame.width * 0.80
        let height = self.view.frame.height * 0.50
        let xPos = self.view.frame.width * 0.10
        let yPos = self.view.frame.height * 0.25
        
        self.loginView = {
            let view = UIView(frame: CGRect(x: xPos, y: yPos, width: width, height: height))
            view.backgroundColor = .white
            view.layer.masksToBounds = true
            view.layer.cornerRadius = padding / 5
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 1.0
            view.layer.shadowOffset = CGSize(width: 2, height: 2)
            view.layer.shadowRadius = 2
            
            view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
            view.layer.shouldRasterize = true
            
            return view
        } ()
        self.view.addSubview(self.loginView)
        
        // Login view contents
        let viewTitle: UILabel = {
            let label = UILabel(frame: CGRect(x: 0, y: padding, width: self.loginView.frame.width, height: self.loginView.frame.height / 12))
            label.text = "Lets do it!"
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: label.frame.height * 0.80, weight: UIFontWeightMedium)
            label.textColor = UIColor(rgb: 0x1774D7)
            
            return label
        }()
        self.loginView.addSubview(viewTitle)
        
        let widthTF = self.loginView.frame.width * 0.90
        let heightTF = self.loginView.frame.height * 0.12
        let xPosTF = self.loginView.frame.width * 0.05
        let yPosTF = viewTitle.frame.maxY + padding
        
        // Username textfield
        self.usernameLoginTF = {
            let textField = UITextField(frame: CGRect(x: xPosTF, y: yPosTF, width: widthTF, height: heightTF))
            textField.layer.masksToBounds = true
            textField.layer.borderColor = UIColor.lightGray.cgColor
            textField.layer.borderWidth = 0.5
            textField.layer.cornerRadius = padding / 5
            
            // textfield icon
            let imgIcon = UIImageView(frame: CGRect(x: padding / 2, y: (textField.frame.height / 2) - (padding * 0.40), width: padding * 0.80, height: padding * 0.80))
            imgIcon.image = #imageLiteral(resourceName: "usernameIcon")
            textField.addSubview(imgIcon)
            
            textField.setLeftPaddingPoints(imgIcon.frame.maxX + (padding * 0.80))
            textField.placeholder = "Username"
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            return textField
        }()
        self.loginView.addSubview(self.usernameLoginTF)
        
        // Password textfield
        self.passwordLoginTF = {
            let textField = UITextField(frame: CGRect(x: xPosTF, y: self.usernameLoginTF.frame.maxY + padding, width: widthTF, height: heightTF))
            textField.layer.masksToBounds = true
            textField.layer.borderColor = UIColor.lightGray.cgColor
            textField.layer.borderWidth = 0.5
            textField.layer.cornerRadius = padding / 5
            
            // textfield icon
            let imgIcon = UIImageView(frame: CGRect(x: padding / 2, y: (textField.frame.height / 2) - (padding * 0.50), width: padding * 0.80, height: padding))
            imgIcon.image = #imageLiteral(resourceName: "passwordIcon")
            textField.addSubview(imgIcon)
            
            textField.setLeftPaddingPoints(imgIcon.frame.maxX + (padding * 0.80))
            textField.placeholder = "Password"
            textField.autocorrectionType = .no
            textField.isSecureTextEntry = true
            
            return textField
        }()
        self.loginView.addSubview(self.passwordLoginTF)
        
        // Login button
        let loginBtn: UIButton = {
            let btn = UIButton(frame: CGRect(x: xPosTF, y: self.passwordLoginTF.frame.maxY + padding, width: widthTF, height: heightTF))
            btn.setBackgroundColor(UIColor(rgb: 0x1774D7), forState: .normal)
            btn.setTitle("Login", for: .normal)
            btn.setTitleColor(.white, for: .normal)
            btn.layer.masksToBounds = true
            btn.layer.cornerRadius = padding / 5
            btn.addTarget(self, action: #selector(loginBtnAction), for: .touchUpInside)
            
            return btn
        } ()
        self.loginView.addSubview(loginBtn)
        
        // Sign up button
        let signUpBtn: UIButton = {
            let btn = UIButton(frame: CGRect(x: xPosTF, y: loginBtn.frame.maxY + padding, width: widthTF, height: heightTF))
            btn.setBackgroundColor(.white, forState: .normal)
            btn.setTitle("Signup", for: .normal)
            btn.setTitleColor(UIColor(rgb: 0x1774D7), for: .normal)
            btn.layer.masksToBounds = true
            btn.layer.borderColor = UIColor(rgb: 0x1774D7).cgColor
            btn.layer.borderWidth = 1.0
            btn.layer.cornerRadius = padding / 5
            btn.addTarget(self, action: #selector(signupBtnAction), for: .touchUpInside)
            
            return btn
        } ()
        self.loginView.addSubview(signUpBtn)
        
        // Forgot password button
        let forgotPassBtn: UIButton = {
            let btn = UIButton(frame: CGRect(x: xPosTF, y: signUpBtn.frame.maxY + padding, width: widthTF / 2, height: heightTF / 2))
            btn.setBackgroundColor(.white, forState: .normal)
            btn.setTitle("Forgot your password?", for: .normal)
            btn.setTitleColor(UIColor(rgb: 0x1774D7), for: .normal)
            btn.titleLabel?.font = UIFont.systemFont(ofSize: btn.frame.height * 0.60)
            btn.contentHorizontalAlignment = .left
            
            return btn
        } ()
        self.loginView.addSubview(forgotPassBtn)
    }
    
    func loginBtnAction () {
        self.loginCheck()
    }
    
    func loginCheck () {
        var errorMsg = ""
        var user = EUsers()
        var success = false
        if let username = self.usernameLoginTF.text, let password = self.passwordLoginTF.text {
            if username == "" {
                errorMsg = "Empty Username"
                success = false
            } else if password == "" {
                errorMsg = "Empty Password"
                success = false
            } else {
                self.users.forEach {
                    u in
                    if u.username == username && u.password == password{
                        user = u
                        success = true
                        self.loginOutput(msg: errorMsg, success: success, user: user)
                        return
                    } else {
                        errorMsg = "Wrong credentials."
                        success = false
                    }
                }
            }
        }
        self.loginOutput(msg: errorMsg, success: success, user: user)
    }
    
    func loginOutput (msg: String, success: Bool, user: EUsers) {
        if success {
            let vc = TabBarController()
            vc.user = user
            UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)
            //UIApplication.shared.keyWindow?.rootViewController?.navigationController?.pushViewController(vc, animated: true)
        } else {
            UIAlertController.showAlert("Login Unsuccessful", message: msg, target: self, completion: nil)
        }
    }
    
    func signupBtnAction () {
        self.loginView.isHidden = true
        self.signUpView.isHidden = false
    }
}

// MARK: - Signup View
extension Login {
    func setupSignUpView () {
        let padding = self.view.frame.width / 20
        let width = self.view.frame.width * 0.80
        let height = self.view.frame.height * 0.62
        let xPos = self.view.frame.width * 0.10
        let yPos = self.view.frame.height * 0.25
        
        self.signUpView = {
            let view = UIView(frame: CGRect(x: xPos, y: yPos, width: width, height: height))
            view.backgroundColor = .white
            view.layer.masksToBounds = true
            view.layer.cornerRadius = padding / 5
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 1.0
            view.layer.shadowOffset = CGSize(width: 2, height: 2)
            view.layer.shadowRadius = 2
            
            view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
            view.layer.shouldRasterize = true
            
            return view
        } ()
        self.view.addSubview(self.signUpView)
        
        // Login view contents
        let viewTitle: UILabel = {
            let label = UILabel(frame: CGRect(x: 0, y: padding, width: self.loginView.frame.width, height: self.loginView.frame.height / 12))
            label.text = "Lets do it!"
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: label.frame.height * 0.80, weight: UIFontWeightMedium)
            label.textColor = UIColor(rgb: 0x1774D7)
            
            return label
        }()
        self.signUpView.addSubview(viewTitle)
        
        let widthTF = self.loginView.frame.width * 0.90
        let heightTF = self.loginView.frame.height * 0.12
        let xPosTF = self.loginView.frame.width * 0.05
        let yPosTF = viewTitle.frame.maxY + padding
        
        // Username textfield
        self.usernameSignupTF = {
            let textField = UITextField(frame: CGRect(x: xPosTF, y: yPosTF, width: widthTF, height: heightTF))
            textField.layer.masksToBounds = true
            textField.layer.borderColor = UIColor.lightGray.cgColor
            textField.layer.borderWidth = 0.5
            textField.layer.cornerRadius = padding / 5
            
            // textfield icon
            let imgIcon = UIImageView(frame: CGRect(x: padding / 2, y: (textField.frame.height / 2) - (padding * 0.40), width: padding * 0.80, height: padding * 0.80))
            imgIcon.image = #imageLiteral(resourceName: "usernameIcon")
            textField.addSubview(imgIcon)
            
            textField.setLeftPaddingPoints(imgIcon.frame.maxX + (padding * 0.80))
            textField.placeholder = "Username"
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            return textField
        }()
        self.signUpView.addSubview(self.usernameSignupTF)
        
        // Email textfield
        self.emailSignupTF = {
            let textField = UITextField(frame: CGRect(x: xPosTF, y: self.usernameSignupTF.frame.maxY + padding, width: widthTF, height: heightTF))
            textField.layer.masksToBounds = true
            textField.layer.borderColor = UIColor.lightGray.cgColor
            textField.layer.borderWidth = 0.5
            textField.layer.cornerRadius = padding / 5
            
            // textfield icon
            let imgIcon = UIImageView(frame: CGRect(x: padding / 2, y: (textField.frame.height / 2) - (padding * 0.50), width: padding * 0.80, height: padding))
            imgIcon.image = #imageLiteral(resourceName: "usernameIcon")
            textField.addSubview(imgIcon)
            
            textField.setLeftPaddingPoints(imgIcon.frame.maxX + (padding * 0.80))
            textField.placeholder = "Email"
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.keyboardType = .emailAddress
            textField.clearButtonMode = .whileEditing
            
            return textField
        }()
        self.signUpView.addSubview(self.emailSignupTF)
        
        // Password textfield
        self.passwordSignupTF = {
            let textField = UITextField(frame: CGRect(x: xPosTF, y: self.emailSignupTF.frame.maxY + padding, width: widthTF, height: heightTF))
            textField.layer.masksToBounds = true
            textField.layer.borderColor = UIColor.lightGray.cgColor
            textField.layer.borderWidth = 0.5
            textField.layer.cornerRadius = padding / 5
            
            // textfield icon
            let imgIcon = UIImageView(frame: CGRect(x: padding / 2, y: (textField.frame.height / 2) - (padding * 0.50), width: padding * 0.80, height: padding))
            imgIcon.image = #imageLiteral(resourceName: "passwordIcon")
            textField.addSubview(imgIcon)
            
            textField.setLeftPaddingPoints(imgIcon.frame.maxX + (padding * 0.80))
            textField.placeholder = "Password"
            textField.autocorrectionType = .no
            textField.isSecureTextEntry = true
            textField.autocapitalizationType = .none
            
            return textField
        }()
        self.signUpView.addSubview(self.passwordSignupTF)
        
        // Re-Enter Password textfield
        self.rePasswordSignupTF = {
            let textField = UITextField(frame: CGRect(x: xPosTF, y: self.passwordSignupTF.frame.maxY + padding, width: widthTF, height: heightTF))
            textField.layer.masksToBounds = true
            textField.layer.borderColor = UIColor.lightGray.cgColor
            textField.layer.borderWidth = 0.5
            textField.layer.cornerRadius = padding / 5
            
            // textfield icon
            let imgIcon = UIImageView(frame: CGRect(x: padding / 2, y: (textField.frame.height / 2) - (padding * 0.50), width: padding * 0.80, height: padding))
            imgIcon.image = #imageLiteral(resourceName: "passwordIcon")
            textField.addSubview(imgIcon)
            
            textField.setLeftPaddingPoints(imgIcon.frame.maxX + (padding * 0.80))
            textField.placeholder = "Re-enter Password"
            textField.autocorrectionType = .no
            textField.isSecureTextEntry = true
            textField.autocapitalizationType = .none
            
            return textField
        }()
        self.signUpView.addSubview(self.rePasswordSignupTF)
        
        // Submit button
        let submitBtn: UIButton = {
            let btn = UIButton(frame: CGRect(x: xPosTF, y: self.rePasswordSignupTF.frame.maxY + padding, width: widthTF, height: heightTF))
            btn.setBackgroundColor(UIColor(rgb: 0x1774D7), forState: .normal)
            btn.setTitle("Submit", for: .normal)
            btn.setTitleColor(.white, for: .normal)
            btn.layer.masksToBounds = true
            btn.layer.cornerRadius = padding / 5
            btn.addTarget(self, action: #selector(submitBtnAction), for: .touchUpInside)
            
            return btn
        } ()
        self.signUpView.addSubview(submitBtn)
        
        // Sign up button
        let cancelBtn: UIButton = {
            let btn = UIButton(frame: CGRect(x: xPosTF, y: submitBtn.frame.maxY + (padding / 2), width: widthTF, height: heightTF))
            btn.setBackgroundColor(.white, forState: .normal)
            btn.setTitle("Cancel", for: .normal)
            btn.setTitleColor(UIColor(rgb: 0x1774D7), for: .normal)
            btn.layer.masksToBounds = true
            btn.layer.borderColor = UIColor(rgb: 0x1774D7).cgColor
            btn.layer.borderWidth = 1.0
            btn.layer.cornerRadius = padding / 5
            btn.addTarget(self, action: #selector(cancelBtnAction), for: .touchUpInside)
            
            return btn
        } ()
        self.signUpView.addSubview(cancelBtn)
        
        self.signUpView.isHidden = true
    }
    
    func submitBtnAction () {
        let checkFields = self.checkFields()
        if checkFields != "" {
            UIAlertController.showAlert("Signup Error", message: checkFields, target: self, completion: nil)
        } else {
            let newUser = EUsers()
            newUser.userId = self.users.count
            newUser.username = self.usernameSignupTF.text!
            newUser.email = self.emailSignupTF.text!
            newUser.password = self.passwordSignupTF.text!
            newUser.rePassword = self.rePasswordSignupTF.text!
            
            try! uiRealm.write {
                uiRealm.add(newUser)
                DispatchQueue.main.async {
                    self.successfullSignup()
                }
            }
        }
    }
    
    func successfullSignup () {
        let alert = UIAlertController(title: "Signup Complete!", message: "Signup successfully completed. Do you want to Login?", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Okay!", style: .default, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            self.loginView.isHidden = false
            self.signUpView.isHidden = true
            
            self.usernameSignupTF.text = ""
            self.emailSignupTF.text = ""
            self.passwordSignupTF.text = ""
            self.rePasswordSignupTF.text = ""
            }})
        alert.addAction(defaultAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            //..
            }})
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelBtnAction () {
        self.loginView.isHidden = false
        self.signUpView.isHidden = true
    }
    
    func checkFields () -> String {
        var errorMsg = ""
        
        if (self.usernameSignupTF.text?.isEmpty)! {
            errorMsg += "Username\n"
        }
        if (self.emailSignupTF.text?.isEmpty)! {
            errorMsg += "Email\n"
        }
        if (self.passwordSignupTF.text?.isEmpty)! {
            errorMsg += "Password\n"
        }
        if (self.rePasswordSignupTF.text?.isEmpty)! {
            errorMsg += "Re-enter Paasword\n"
        }
        
        if errorMsg != "" {
            errorMsg = "Empty fields:\n" + errorMsg
        }
        
        if self.rePasswordSignupTF.text != self.passwordSignupTF.text {
            errorMsg += "\nPassword and Re-enter Password mismatch."
        }
        
        return errorMsg
    }
}

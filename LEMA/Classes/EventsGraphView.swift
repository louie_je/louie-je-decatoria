//
//  EventsGraphView.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 30/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import Foundation
import UIKit

class EventsGraphView: UIView {
    
    var width = CGFloat()
    var padding = CGFloat()
    var yAxisView = UIView()
    var xAxisView = UIView()
    
    var btnPoints = [UIButton]()
    
    var user = EUsers()
    var filterType = filterEvent.date
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.width = UIScreen.main.bounds.width
        self.padding = self.width / 20
        self.frame.size.width = 0
        self.frame.size.height = self.width
        self.setupYAxis()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("CampaignsTabContent is not NSCoding compliant")
    }
}

// MARK: - Setup X Axis
extension EventsGraphView {
    func setupYAxis () {
        let spaceYpoint = self.frame.height / 10
        let yPoints = ["0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000"]
        
        self.yAxisView = UIView(frame: CGRect(x: padding / 2, y: 0, width: padding * 2, height: self.frame.height))
        yAxisView.addRightBorderWithColor(color: UIColor.black, width: 1.0)
        for (i, point) in yPoints.reversed().enumerated() {
            let label = UILabel(frame: CGRect(x: 0, y: CGFloat(i) * spaceYpoint, width: padding * 1.5, height: padding))
            label.text = point
            label.textColor = .black
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: label.frame.height / 2, weight: UIFontWeightMedium)
            yAxisView.addSubview(label)
        }
        self.frame.size.width += yAxisView.frame.width
        self.addSubview(yAxisView)
    }
    
    func setupData (filter: filterEvent) {
        switch filter {
        case .name:
            self.setupXAxis(.name)
        case .date:
            self.setupXAxis(.date)
        case .cost:
            self.setupXAxis(.cost)
        default:
            self.setupXAxis(.name)
        }
    }
    
    func setupXAxis (_ filter: filterEvent) {
        let spaceXdates = (self.width - (padding * 2)) / 4
        
        var events = [EEvents]()
        let res = self.user.events.sorted(byKeyPath: filter.rawValue, ascending: true)
        res.forEach { e in events.append(e)}
        
        var dates = [Date]()
        events.forEach {
            event in
            dates.append(event.date)
        }
        
        self.xAxisView = UIView(frame: CGRect(x: self.yAxisView.frame.maxX, y: self.yAxisView.frame.maxY, width: spaceXdates * 4, height: padding * 2))
        var lineFrom = CGPoint(x: self.yAxisView.frame.maxX, y: self.frame.height)
        for (i, date) in dates.enumerated() {
            let label = UILabel(frame: CGRect(x: (CGFloat(i + 1) * spaceXdates) - (padding * 1.5), y: 0, width: padding * 3, height: padding * 2))
            label.text = date.getMonthAndDayName()
            label.textColor = .black
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: label.frame.height / 2.5, weight: UIFontWeightBold)
            xAxisView.addSubview(label)
            
            if i > 4 {
                self.xAxisView.frame.size.width += label.frame.maxX
            }
            let btnWidth = padding
            let btnYPos = self.frame.height * (1 - (CGFloat(events[i].cost) / 1000))
            let btnXPos = self.xAxisView.frame.origin.x + ((CGFloat(i + 1) * spaceXdates) - (btnWidth / 2))
            
            let btnPoint = UIButton(frame: CGRect(x: btnXPos, y: btnYPos, width: btnWidth, height: btnWidth))
            btnPoint.setBackgroundColor(UIColor.lightGray, forState: .normal)
            btnPoint.layer.masksToBounds = true
            btnPoint.layer.cornerRadius = btnWidth / 2
            btnPoint.tag = i
            self.btnPoints.append(btnPoint)
            self.addSubview(btnPoint)
            
            self.addLine(fromPoint: lineFrom, toPoint: btnPoint.center)
            
            lineFrom = btnPoint.center
        }
        self.btnPoints.forEach {
            btn in
            self.bringSubview(toFront: btn)
        }
        self.frame.size.width += xAxisView.frame.width * 1.2
        self.frame.size.height = xAxisView.frame.maxY
        self.addSubview(xAxisView)
    }
    
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = UIColor(rgb: 0x1774D7).cgColor
        line.lineWidth = padding / 4
        line.lineJoin = kCALineJoinRound
        self.layer.addSublayer(line)
    }
}

// MARK: - Add data
extension EventsGraphView {
    func addData () {
    }
}

//
//  EditEvent.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 29/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import UIKit

class EditEvent: BaseViewController, DatePickerDelegate {

    var user = EUsers()
    var event = EEvents()
    var date = Date()
    var selectedDate = String()
    var cost = Double()
    
    var nameTF = UITextField()
    var costTF = UITextField()
    var dateTF = UITextField()
    var addressTF = UITextField()
    var contactnoTF = UITextField()
    
    var padding = CGFloat()
    var navBarHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.padding = self.view.frame.width / 20
        self.navBarHeight = (self.navigationController?.navigationBar.frame.height)!
        
        self.date = self.event.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        
        let result = dateFormatter.string(from: self.date)
        self.selectedDate = result
        
        self.setupNavBar()
        self.setupFields()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
// MARK: - Setup navigation bar
extension EditEvent {
    func setupNavBar () {
        //Navigation title
        self.navigationItem.title = "Edit Event"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgb: 0x1774D7)]
        
        // Save button
        let saveBtnItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveBtnAction))
        self.navigationItem.rightBarButtonItem = saveBtnItem
    }
    
    func saveBtnAction () {
        if self.textFieldCheck() {
            let eventID = self.event.eventId
            let newEvent = uiRealm.objects(EEvents.self).filter("eventId == \(eventID)").first
            
            try! uiRealm.write {
                let str = self.costTF.text!
                if let c = Double(str) {
                    self.cost = c
                }
                
                newEvent!.name = self.nameTF.text!
                newEvent!.cost = self.cost
                newEvent!.date = self.date
                newEvent!.address = self.addressTF.text!
                newEvent!.contactno = self.contactnoTF.text!
                
                
            }
            DispatchQueue.main.async {
                self.succefullAddEvent()
            }
        } else {
            UIAlertController.showAlert("Edit Event", message: "Some fields are empty. Please provide with with desired data", target: self, completion: nil)
        }
    }
    
    func textFieldCheck () -> Bool {
        if nameTF.text == "" {
            return false
        }
        if costTF.text == "" {
            return false
        }
        if dateTF.text == "" {
            return false
        }
        if addressTF.text == "" {
            return false
        }
        if contactnoTF.text == "" {
            return false
        }
        
        return true
    }
    
    func succefullAddEvent () {
        
        let alert = UIAlertController(title: "Update Complete!", message: "Update successfully completed. Continue update??", preferredStyle: .alert)
        let gotoEventAction = UIAlertAction(title: "Go To Events!", style: .default, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            self.nameTF.text = ""
            self.costTF.text = ""
            self.dateTF.text = ""
            self.contactnoTF.text = ""
            self.addressTF.text = ""
            self.popNavigation()
            }})
        alert.addAction(gotoEventAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            }})
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Setup fields
extension EditEvent {
    func setupFields () {
        // Name
        self.nameTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.navBarHeight + (padding * 2), width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Name"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            textField.text = self.event.name
            
            return textField
        } ()
        self.view.addSubview(self.nameTF)
        
        // Cost
        self.costTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.nameTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Cost"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.keyboardType = .default
            textField.text = "$\(self.event.cost)K"
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            textField.delegate = self
            
            return textField
        } ()
        self.view.addSubview(self.costTF)
        
        // Name
        self.dateTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.costTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Date"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.isUserInteractionEnabled = false
            textField.text = self.selectedDate
            
            return textField
        } ()
        self.view.addSubview(self.dateTF)
        // Date as button to Date Picker
        let dateBtn: UIButton = {
            let btn = UIButton(frame: CGRect(x: padding, y: self.costTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            btn.addTarget(self, action: #selector(toDatePicker), for: .touchUpInside)
            
            return btn
        } ()
        self.view.addSubview(dateBtn)
        
        // Name
        self.addressTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.dateTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Address"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            textField.text = self.event.address
            
            return textField
        } ()
        self.view.addSubview(self.addressTF)
        
        // Name
        self.contactnoTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.addressTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Contact Number"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.keyboardType = .namePhonePad
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            textField.text = self.event.contactno
            
            return textField
        } ()
        self.view.addSubview(self.contactnoTF)
        
        // Delete
        let deleteBtn: UIButton = {
            let btn = UIButton(frame: CGRect(x: self.view.center.x - (padding * 2), y: self.contactnoTF.frame.maxY + (padding * 2), width: padding * 4, height: padding))
            btn.setTitle("Delete", for: .normal)
            btn.setTitleColor(UIColor.red, for: .normal)
            btn.addTarget(self, action: #selector(deleteEvent), for: .touchUpInside)
            
            return btn
        }()
        self.view.addSubview(deleteBtn)
    }
    
    func deleteEvent() {
        let alert = UIAlertController(title: "Delete Event", message: "Would you like to delete this event?", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Yes!", style: .default, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            
            try! uiRealm.write {
                uiRealm.delete(self.event)
            }
            self.popNavigation()
            }})
        alert.addAction(defaultAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {

            }})
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func toDatePicker () {
        let vc = DatePicker()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func selectDate(date: Date, selectedDate: String) {
        self.dateTF.text = "\(selectedDate)"
        self.date = date
    }
}
extension EditEvent: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.costTF {
            var str = self.costTF.text!
            if let c = Double(str) {
                self.cost = c
            }
            if str != "" {
                str = "$\(str)K"
            }
            self.costTF.text = str
            
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.costTF {
            var str = self.costTF.text
            str = str?.replacingOccurrences(of: "$", with: "")
            str = str?.replacingOccurrences(of: "K", with: "")
            self.costTF.text = str
        }
        return true
    }
}

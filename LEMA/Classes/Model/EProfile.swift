//
//  EProfile.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 31/08/2017.
//  Copyright © 2017 Ohmyhome. All rights reserved.
//

import Foundation

import Foundation
import RealmSwift

class EProfile: Object {
    dynamic var profileId = Int()
    dynamic var image = NSData()
    dynamic var name = String()
    dynamic var email = String()
    dynamic var birthdate = Date()
    dynamic var address = String()
    dynamic var contactno = String()
    dynamic var user: EUsers?
}

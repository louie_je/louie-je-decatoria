//
//  EUsers.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 30/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import Foundation
import RealmSwift

class EUsers: Object {
    dynamic var userId = Int()
    dynamic var username = String()
    dynamic var email = String()
    dynamic var password = String()
    dynamic var rePassword = String()
    dynamic var profile: EProfile?
    let events = List<EEvents>()
}

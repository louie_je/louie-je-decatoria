//
//  EEvents.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 30/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import Foundation
import RealmSwift

class EEvents: Object {
    dynamic var eventId = Int()
    dynamic var name = String()
    dynamic var cost = Double()
    dynamic var date = Date()
    dynamic var address = String()
    dynamic var contactno = String()
    dynamic var user: EUsers?
}

//
//  DatePicker.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 29/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import UIKit

protocol DatePickerDelegate: class {
    func selectDate(date: Date, selectedDate: String)
}

class DatePicker: UIViewController {
    
    weak var delegate: DatePickerDelegate? = nil
    
    var date = Date()
    var calendar = Calendar.current
    
    var selectedDateLabel = UILabel()
    
    var selectedDate = String()
    var monthBtn = UIButton()
    var dayBtn = UIButton()
    var yearBtn = UIButton()
    
    var monthDropdown = UIView()
    var dayDropdown = UIView()
    var yearDropdown = UIView()
    
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    
    var activateYear = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        
        let result = dateFormatter.string(from: self.date)
        self.selectedDate = result
        
        self.setupNavBar()
        self.setupViews()
        self.setupDateDropdownViews()
    }
    
    func setupNavBar () {
        //Navigation title
        self.navigationItem.title = "Date Picker"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgb: 0x1774D7)]
        
        // Save button
        let saveBtnItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveBtnAction))
        self.navigationItem.rightBarButtonItem = saveBtnItem
    }
    
    func saveBtnAction () {
        self.delegate?.selectDate(date: self.date, selectedDate: self.selectedDate)
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
// MARK: - Setup Views
extension DatePicker {
    func setupViews () {
        let padding = self.view.frame.width / 20
        let year = calendar.component(.year, from: date)
        let day = calendar.component(.day, from: date)
        
        // Selected Date
        self.selectedDateLabel = UILabel(frame: CGRect(x: 0, y: self.view.frame.height / 8, width: self.view.frame.width, height: padding * 2))
        self.selectedDateLabel.text = self.selectedDate
        self.selectedDateLabel.font = UIFont.systemFont(ofSize: self.selectedDateLabel.frame.height, weight: UIFontWeightBold)
        
        self.selectedDateLabel.textAlignment = .center
        self.view.addSubview(self.selectedDateLabel)
        
        // Date
        // Day
        self.dayBtn = {
            let btn = UIButton(frame: CGRect(x: self.view.center.x - padding, y: selectedDateLabel.frame.maxY + padding, width: padding * 2, height: padding * 1.5))
            btn.setTitle("\(day)", for: .normal)
            btn.setTitleColor(UIColor.black, for: .normal)
            btn.addTarget(self, action: #selector(selectDay), for: .touchUpInside)
            btn.layer.masksToBounds = true
            btn.layer.cornerRadius = btn.frame.height / 10
            btn.layer.borderWidth = 0.5
            btn.layer.borderColor = UIColor.lightGray.cgColor
            
            return btn
        } ()
        self.view.addSubview(self.dayBtn)
        
        // Month
        self.monthBtn = {
            let btn = UIButton(frame: CGRect(x: self.dayBtn.frame.origin.x - (padding * 3.2), y: selectedDateLabel.frame.maxY + padding, width: padding * 3, height: padding * 1.5))
            btn.setTitle(self.date.getMonthName(), for: .normal)
            btn.addTarget(self, action: #selector(selectMonth), for: .touchUpInside)
            btn.setTitleColor(UIColor.black, for: .normal)
            btn.layer.masksToBounds = true
            btn.layer.cornerRadius = btn.frame.height / 10
            btn.layer.borderWidth = 0.5
            btn.layer.borderColor = UIColor.lightGray.cgColor
            
            return btn
        } ()
        self.view.addSubview(self.monthBtn)
        
        // Year
        self.yearBtn = {
            let btn = UIButton(frame: CGRect(x: self.dayBtn.frame.maxX + (padding * 0.2), y: selectedDateLabel.frame.maxY + padding, width: padding * 3, height: padding * 1.5))
            btn.setTitle("\(year)", for: .normal)
            btn.addTarget(self, action: #selector(selectYear), for: .touchUpInside)
            btn.setTitleColor(UIColor.black, for: .normal)
            btn.layer.masksToBounds = true
            btn.layer.cornerRadius = btn.frame.height / 10
            btn.layer.borderWidth = 0.5
            btn.layer.borderColor = UIColor.lightGray.cgColor
            
            return btn
        } ()
        self.view.addSubview(self.yearBtn)
    }
    
    func selectMonth () {
        if self.monthDropdown.isHidden {
            self.monthDropdown.isHidden = false
            self.dayDropdown.isHidden = true
            self.yearDropdown.isHidden = true
        } else {
            self.monthDropdown.isHidden = true
        }
    }
    
    func selectDay () {
        if self.dayDropdown.isHidden {
            self.dayDropdown.isHidden = false
            self.monthDropdown.isHidden = true
            self.yearDropdown.isHidden = true
        } else {
            self.dayDropdown.isHidden = true
        }
    }
    
    func selectYear () {
        if self.activateYear {
            if self.yearDropdown.isHidden {
                self.dayDropdown.isHidden = true
                self.monthDropdown.isHidden = true
                self.yearDropdown.isHidden = false
            } else {
                self.yearDropdown.isHidden = true
            }
        }
    }
}

// MARK: - Dropdown views
extension DatePicker {
    func setupDateDropdownViews () {
        self.monthDropdown = {
            let padding = self.view.frame.width / 20
            let view = UIView(frame: CGRect(x: self.monthBtn.frame.origin.x, y: self.monthBtn.frame.maxY + 2, width: self.monthBtn.frame.width, height: 0))
            view.layer.masksToBounds = true
            view.layer.cornerRadius = self.yearBtn.layer.cornerRadius
            view.layer.borderWidth = 0.5
            view.layer.borderColor = UIColor.lightGray.cgColor
            
            for (i, mon) in self.months.enumerated() {
                let btn = UIButton(frame: CGRect(x: 0, y: CGFloat(i) * (padding * 1.4), width: view.frame.width, height: padding))
                btn.addTarget(self, action: #selector(selectedMonth(_:)), for: .touchUpInside)
                btn.setTitle(mon, for: .normal)
                btn.setTitleColor(UIColor.black, for: .normal)
                btn.tag = i
                view.addSubview(btn)
                view.frame.size.height = btn.frame.maxY
            }
            return view
        } ()
        self.monthDropdown.isHidden = true
        self.view.addSubview(self.monthDropdown)
        
        self.dayDropdown = {
            let padding = self.view.frame.width / 20
            let view = UIView(frame: CGRect(x: self.dayBtn.frame.origin.x - (self.dayBtn.frame.width), y: self.dayBtn.frame.maxY + 2, width: self.dayBtn.frame.width * 3, height: 0))
            view.layer.masksToBounds = true
            view.layer.cornerRadius = self.yearBtn.layer.cornerRadius
            view.layer.borderWidth = 0.5
            view.layer.borderColor = UIColor.lightGray.cgColor
            
            for i in 0..<31 {
                let y = i % 10
                let x = i / 10
                let width = view.frame.width / 3
                let btn = UIButton(frame: CGRect(x: CGFloat(x) * width, y: CGFloat(y) * (padding * 1.4), width: width, height: padding))
                btn.addTarget(self, action: #selector(selectedDay(_:)), for: .touchUpInside)
                btn.setTitle("\(i + 1)", for: .normal)
                btn.setTitleColor(UIColor.black, for: .normal)
                btn.tag = i + 1
                view.addSubview(btn)
                view.frame.size.height = (padding * 1.4) * 10
            }
            return view
        } ()
        self.dayDropdown.isHidden = true
        self.view.addSubview(self.dayDropdown)
        
        self.yearDropdown = {
            let padding = self.view.frame.width / 20
            let view = UIView(frame: CGRect(x: self.yearBtn.frame.origin.x - (self.yearBtn.frame.width), y: self.yearBtn.frame.maxY + 2, width: self.yearBtn.frame.width * 3, height: 0))
            view.layer.masksToBounds = true
            view.layer.cornerRadius = self.yearBtn.layer.cornerRadius
            view.layer.borderWidth = 0.5
            view.layer.borderColor = UIColor.lightGray.cgColor
            
            for i in 0..<31 {
                let year = 1980 + i
                let y = i % 10
                let x = i / 10
                let width = view.frame.width / 3
                let btn = UIButton(frame: CGRect(x: CGFloat(x) * width, y: CGFloat(y) * (padding * 1.4), width: width, height: padding))
                btn.addTarget(self, action: #selector(selectedYear(_:)), for: .touchUpInside)
                btn.setTitle("\(year)", for: .normal)
                btn.setTitleColor(UIColor.black, for: .normal)
                btn.tag = year
                view.addSubview(btn)
                view.frame.size.height = (padding * 1.4) * 10
            }
            return view
        } ()
        self.yearDropdown.isHidden = true
        self.view.addSubview(self.yearDropdown)
    }
    
    func selectedMonth (_ sender: UIButton) {
        if let month = sender.titleLabel?.text {
            self.monthBtn.setTitle(month, for: .normal)
        }
        var components = self.calendar.dateComponents([.year, .month, .day], from: self.date)
        components.month = sender.tag + 1
        self.date = self.calendar.date(from: components)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        let result = dateFormatter.string(from: self.date)
        self.selectedDate = result
        
        self.selectedDateLabel.text = self.selectedDate
        self.monthDropdown.isHidden = true
    }
    
    func selectedDay (_ sender: UIButton) {
        if let day = sender.titleLabel?.text {
            self.dayBtn.setTitle(day, for: .normal)
        }
        var components = self.calendar.dateComponents([.year, .month, .day], from: self.date)
        components.day = sender.tag
        self.date = self.calendar.date(from: components)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        let result = dateFormatter.string(from: self.date)
        self.selectedDate = result
        
        self.selectedDateLabel.text = self.selectedDate
        self.dayDropdown.isHidden = true
    }
    
    func selectedYear (_ sender: UIButton) {
        if let year = sender.titleLabel?.text {
            self.yearBtn.setTitle(year, for: .normal)
        }
        var components = self.calendar.dateComponents([.year, .month, .day], from: self.date)
        components.year = sender.tag
        self.date = self.calendar.date(from: components)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        let result = dateFormatter.string(from: self.date)
        self.selectedDate = result
        
        self.selectedDateLabel.text = self.selectedDate
        self.yearDropdown.isHidden = true
    }
}

//
//  AddEvent.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 29/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import UIKit

class AddEvent: BaseViewController, DatePickerDelegate {
    
    var user = EUsers()
    var date = Date()
    var cost = Double()
    
    var nameTF = UITextField()
    var costTF = UITextField()
    var dateTF = UITextField()
    var addressTF = UITextField()
    var contactnoTF = UITextField()
    
    var padding = CGFloat()
    var navBarHeight = CGFloat()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.padding = self.view.frame.width / 20
        self.navBarHeight = (self.navigationController?.navigationBar.frame.height)!
        
        self.setupNavBar()
        self.setupFields()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
// MARK: - Setup navigation bar
extension AddEvent {
    func setupNavBar () {
        //Navigation title
        self.navigationItem.title = "Create Event"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgb: 0x1774D7)]
        
        // Save button
        let saveBtnItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveBtnAction))
        self.navigationItem.rightBarButtonItem = saveBtnItem
    }
    
    func saveBtnAction () {
        if self.textFieldCheck() {
            let newEvent = EEvents()
            let str = self.costTF.text!
            if let c = Double(str) {
                self.cost = c
            }
            
            newEvent.name = self.nameTF.text!
            newEvent.cost = self.cost
            newEvent.date = self.date
            newEvent.address = self.addressTF.text!
            newEvent.contactno = self.contactnoTF.text!
            newEvent.user = self.user
            newEvent.eventId = self.user.events.count
            
            try! uiRealm.write {
                self.user.events.append(newEvent)
            }
            DispatchQueue.main.async {
                self.succefullAddEvent()
            }
        } else {
            UIAlertController.showAlert("Add Event", message: "Some fields are empty. Please provide with with desired data", target: self, completion: nil)
        }
    }
    
    func textFieldCheck () -> Bool {
        if nameTF.text == "" {
            return false
        }
        if costTF.text == "" {
            return false
        }
        if dateTF.text == "" {
            return false
        }
        if addressTF.text == "" {
            return false
        }
        if contactnoTF.text == "" {
            return false
        }
        
        return true
    }
    
    func succefullAddEvent () {
        
        let alert = UIAlertController(title: "Event Added!", message: "Event completely added. Would like to add more?", preferredStyle: .alert)
        let gotoEventAction = UIAlertAction(title: "Go To Events!", style: .default, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            self.nameTF.text = ""
            self.costTF.text = ""
            self.dateTF.text = ""
            self.contactnoTF.text = ""
            self.addressTF.text = ""
            self.popNavigation()
            }})
        alert.addAction(gotoEventAction)
        let addEventAction = UIAlertAction(title: "Add Event", style: .default, handler: {(alert: UIAlertAction!) in DispatchQueue.main.async {
            self.nameTF.text = ""
            self.costTF.text = ""
            self.dateTF.text = ""
            self.contactnoTF.text = ""
            self.addressTF.text = ""
            self.nameTF.becomeFirstResponder()
            }})
        alert.addAction(addEventAction)
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Setup fields
extension AddEvent {
    func setupFields () {
        // Name
        self.nameTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.navBarHeight + (padding * 2), width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Name"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            return textField
        } ()
        self.view.addSubview(self.nameTF)
        
        // Cost
        self.costTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.nameTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Cost ($ in thousands)"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.keyboardType = .decimalPad
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            textField.delegate = self
            
            return textField
        } ()
        self.view.addSubview(self.costTF)
        
        // Date
        self.dateTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.costTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Date"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.isUserInteractionEnabled = false
            
            return textField
        } ()
        self.view.addSubview(self.dateTF)
        // Date as button to Date Picker
        let dateBtn: UIButton = {
            let btn = UIButton(frame: CGRect(x: padding, y: self.costTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            btn.addTarget(self, action: #selector(toDatePicker), for: .touchUpInside)
            
            return btn
        } ()
        self.view.addSubview(dateBtn)
        
        // Address
        self.addressTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.dateTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Address"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            return textField
        } ()
        self.view.addSubview(self.addressTF)
        
        // contact no
        self.contactnoTF = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.addressTF.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Contact Number"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.keyboardType = .namePhonePad
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            return textField
        } ()
        self.view.addSubview(self.contactnoTF)
    }
    
    func toDatePicker () {
        let vc = DatePicker()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func selectDate(date: Date, selectedDate: String) {
        self.dateTF.text = "\(selectedDate)"
        self.date = date
    }
}

extension AddEvent: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.costTF {
            var str = self.costTF.text!
            if let c = Double(str) {
                self.cost = c
            }
            if str != "" {
                str = "$\(str)K"
            }
            self.costTF.text = str
            
            if self.cost > 1000 {
                UIAlertController.showAlert("Add Event", message: "Cost of an event will not be greater than 1000, we will set the cost to 999 as the maximum cost.", target: self, completion: {
                    self.cost = 999
                    self.costTF.text = "$\(999)K"
                })
            }
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.costTF {
            var str = self.costTF.text
            str = str?.replacingOccurrences(of: "$", with: "")
            str = str?.replacingOccurrences(of: "K", with: "")
            self.costTF.text = str
        }
        return true
    }
}

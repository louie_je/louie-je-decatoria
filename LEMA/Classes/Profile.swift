//
//  Profile.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 30/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import UIKit

class Profile: BaseViewController {
    
    var user = EUsers()
    var birthDateValue = Date()
    var hasProfile = false
    
    var padding = CGFloat()
    
    var profileImg = UIImageView()
    var name = UITextField()
    var email = UITextField()
    var birthdate = UITextField()
    var dateBtn = UIButton()
    var address = UITextField()
    var contactNo = UITextField()
    
    var photoBtn = UIButton()
    
    var navBarHeight = CGFloat()

    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBar = self.tabBarController as! TabBarController
        self.user = tabBar.user
        if self.user.profile  == nil {
            self.hasProfile = false
        } else { self.hasProfile = true }
        
        self.padding = self.view.frame.width / 20
        self.navBarHeight = (self.navigationController?.navigationBar.frame.height)!
        self.setupNavBar()
        
        self.setupViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
// MARK: - Setup navigation bar
extension Profile {
    func setupNavBar () {
        //Navigation title
        self.navigationItem.title = "Profile"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgb: 0x1774D7)]
        
        // Save button
        let saveBtnItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveBtnAction))
        let updateBtnItem = UIBarButtonItem(title: "Update", style: .done, target: self, action: #selector(updateBtnAction))
        
        if hasProfile {
            self.navigationItem.rightBarButtonItem = updateBtnItem
        } else {
            self.navigationItem.rightBarButtonItem = saveBtnItem
        }
        
    }
    
    func saveBtnAction () {
        if self.textFieldCheck() {
            let profile = EProfile()
            
            if let img = self.profileImg.image {
                let imgData = NSData(data: UIImageJPEGRepresentation(img, 1)!)
                profile.image = imgData
            }
            
            profile.name = self.name.text!
            profile.email = self.email.text!
            profile.birthdate = self.birthDateValue
            profile.address = self.address.text!
            profile.contactno = self.contactNo.text!
            
            try! uiRealm.write {
                self.user.profile = profile
            }
            DispatchQueue.main.async {
                var msg = ""
                if self.hasProfile { msg = "Successfully updated your profile" }
                else { msg = "Successfully added your profile" }
                
                UIAlertController.showAlert("Profile", message: msg, target: self, completion: nil)
                self.view.endEditing(true)
                
                self.photoBtn.isHidden = true
                self.name.isUserInteractionEnabled = false
                self.email.isUserInteractionEnabled = false
                self.birthdate.isUserInteractionEnabled = false
                self.address.isUserInteractionEnabled = false
                self.contactNo.isUserInteractionEnabled = false
                
                self.navigationItem.rightBarButtonItem = nil
                
                let saveBtnItem = UIBarButtonItem(title: "Update", style: .done, target: self, action: #selector(self.updateBtnAction))
                self.navigationItem.rightBarButtonItem = saveBtnItem
            }
        } else {
            UIAlertController.showAlert("Profile", message: "Some fields are empty. Please provide with with desired data", target: self, completion: nil)
        }
        
        
    }
    
    func textFieldCheck () -> Bool {
        if name.text == "" {
            return false
        }
        if email.text == "" {
            return false
        }
        if birthdate.text == "" {
            return false
        }
        if address.text == "" {
            return false
        }
        if contactNo.text == "" {
            return false
        }
        
        return true
    }
    
    func updateBtnAction () {
        self.photoBtn.isHidden = false
        self.name.isUserInteractionEnabled = true
        self.email.isUserInteractionEnabled = true
        self.birthdate.isUserInteractionEnabled = true
        self.dateBtn.isUserInteractionEnabled = true
        self.address.isUserInteractionEnabled = true
        self.contactNo.isUserInteractionEnabled = true
        
        self.name.becomeFirstResponder()
        
        self.navigationItem.rightBarButtonItem = nil
        
        let saveBtnItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveBtnAction))
        self.navigationItem.rightBarButtonItem = saveBtnItem
    }
}


// MARK: - Setup Views 
extension Profile: DatePickerDelegate {
    func setupViews () {
        // Profile Pic
        self.profileImg = {
            let imgView = UIImageView(frame: CGRect(x: self.view.center.x - (padding * 2.5), y: self.navBarHeight + (padding * 2), width: padding * 5, height: padding * 5))
            imgView.backgroundColor = .lightGray
            imgView.layer.masksToBounds = true
            imgView.layer.cornerRadius = imgView.frame.height / 2
            
            return imgView
        } ()
        self.view.addSubview(self.profileImg)
        
        // Photo btn
        self.photoBtn = {
            let btn = UIButton(frame: CGRect(x: self.profileImg.center.x - padding, y: self.profileImg.center.y - padding, width: padding * 2, height: padding * 1.5))
            btn.setBackgroundImage(#imageLiteral(resourceName: "camera"), for: .normal)
            btn.addTarget(self, action: #selector(photoBtnAction), for: .touchUpInside)
            return btn
        }()
        self.view.addSubview(self.photoBtn)
        
        // Name
        self.name = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.profileImg.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Name"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            return textField
        } ()
        self.view.addSubview(self.name)
        
        // Name
        self.email = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.name.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Email"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.keyboardType = .emailAddress
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            return textField
        } ()
        self.view.addSubview(self.email)
        
        // Name
        self.birthdate = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.email.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Date of Birth"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            
            
            return textField
        } ()
        self.view.addSubview(self.birthdate)
        // Date as button to Date Picker
        self.dateBtn = {
            let btn = UIButton(frame: CGRect(x: padding, y: self.email.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            btn.addTarget(self, action: #selector(toDatePicker), for: .touchUpInside)
            
            return btn
        } ()
        self.view.addSubview(self.dateBtn)
        
        // Name
        self.address = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.birthdate.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Address"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            return textField
        } ()
        self.view.addSubview(self.address)
        
        // Contact No
        self.contactNo = {
            let textField = UITextField(frame: CGRect(x: padding, y: self.address.frame.maxY + padding, width: self.view.frame.width - (padding * 2), height: padding * 2))
            textField.placeholder = "Contact Number"
            textField.placeHolderColor = UIColor.black
            textField.addBottomBorder(thickness: 1, color: UIColor.lightGray)
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.clearButtonMode = .whileEditing
            
            
            return textField
        } ()
        self.view.addSubview(self.contactNo)
        
        if self.hasProfile {
            let profile = self.user.profile
            
            if let imgData = profile?.image {
                let img = UIImage(data: imgData as Data)
                self.profileImg.image = img
            }
            
            self.name.text = profile?.name
            self.email.text = profile?.email
            self.birthdate.text = profile?.birthdate.getFullDate()
            self.address.text = profile?.address
            self.contactNo.text = profile?.contactno
            
            self.photoBtn.isHidden = true
            self.name.isUserInteractionEnabled = false
            self.email.isUserInteractionEnabled = false
            self.birthdate.isUserInteractionEnabled = false
            self.dateBtn.isUserInteractionEnabled = false
            self.address.isUserInteractionEnabled = false
            self.contactNo.isUserInteractionEnabled = false
        } else {
            self.photoBtn.isHidden = false
            self.name.isUserInteractionEnabled = true
            self.email.isUserInteractionEnabled = true
            self.birthdate.isUserInteractionEnabled = true
            self.dateBtn.isUserInteractionEnabled = true
            self.address.isUserInteractionEnabled = true
            self.contactNo.isUserInteractionEnabled = true
        }
        
        
    }
}

// MARK: Date Picker delegate
extension Profile {
    func toDatePicker () {
        let vc = DatePicker()
        vc.delegate = self
        vc.activateYear = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func selectDate(date: Date, selectedDate: String) {
        self.birthdate.text = "\(selectedDate)"
        self.birthDateValue = date
    }
}

// MARK: Image Picker delegate
extension Profile: ImagePickerDelegate {
    func photoBtnAction () {
        let vc = ImagePicker()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func selectImage(image: UIImage) {
        self.profileImg.image = image
    }
}

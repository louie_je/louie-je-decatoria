//
//  ImagePicker.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 04/09/2017.
//  Copyright © 2017 Ohmyhome. All rights reserved.
//

import Foundation
import UIKit
import Photos

protocol ImagePickerDelegate: class {
    func selectImage(image: UIImage)
}

class ImagePicker: BaseViewController {
    
    weak var delegate: ImagePickerDelegate? = nil
    
    var padding = CGFloat()
    
    var photosCollection: UICollectionView!
    
    var selectedImg = UIImageView()
    
    var imgViewer = UIImageView()
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.padding = self.view.frame.width / 20
        
        self.setupNavBar()
        self.setupViews ()
        self.setData()
    }
    
}

// MARK: - Setup navigation bar
extension ImagePicker {
    func setupNavBar () {
        //Navigation title
        self.navigationItem.title = "Photos"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgb: 0x1774D7)]
        
        // Save button
        let saveBtnItem = UIBarButtonItem(title: "Select", style: .done, target: self, action: #selector(selectBtnAction))
        self.navigationItem.rightBarButtonItem = saveBtnItem
    }
    
    func selectBtnAction () {
        if self.imgViewer.image == nil {
            UIAlertController.showAlert("Image Picker", message: "We have notice you didnt pick an image or took from camera.", target: self, completion: nil)
        } else {
            if let img = self.imgViewer.image {
                self.delegate?.selectImage(image: img)
                self.navigationController?.popViewController(animated: true)
            } else { self.navigationController?.popViewController(animated: true) }
        }
        
    }
    
    func setupViews () {
        self.imgViewer = {
            let imgView = UIImageView(frame: CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.maxY)!, width: self.view.frame.height / 3, height: self.view.frame.height / 3))
            imgView.center.x = self.view.center.x
            imgView.layer.masksToBounds = true
            imgView.layer.cornerRadius = imgView.frame.height / 2
            imgView.backgroundColor = .lightGray
            
            return imgView
        }()
        self.view.addSubview(self.imgViewer)
        
        let takePictureButton: UIButton = {
            let btn = UIButton(frame: CGRect(x: self.imgViewer.center.x - (padding * 4), y: self.imgViewer.frame.maxY + padding, width: padding * 3, height: padding * 2.5))
            btn.setBackgroundImage(#imageLiteral(resourceName: "camera"), for: .normal)
            btn.addTarget(self, action: #selector(openCamera), for: .touchUpInside)
            
            return btn
        }()
        self.view.addSubview(takePictureButton)
        
        let openGalleryButton: UIButton = {
            let btn = UIButton(frame: CGRect(x: self.imgViewer.center.x + padding, y: self.imgViewer.frame.maxY + padding, width: padding * 3, height: padding * 2.5))
            btn.setBackgroundImage(#imageLiteral(resourceName: "photoGallery"), for: .normal)
            btn.addTarget(self, action: #selector(openPhotoLibrary), for: .touchUpInside)
            
            return btn
        }()
        self.view.addSubview(openGalleryButton)
    }
    
    func openCamera() {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            UIAlertController.showAlert("Image Picker", message: "This device doesn't have a camera.", target: self, completion: nil)
            return
        }
        
        self.imagePicker.sourceType = .camera
        self.imagePicker.cameraDevice = .rear
        self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for:.camera)!
        self.imagePicker.delegate = self
        
        self.present(self.imagePicker, animated: true)
    }
    
    func openPhotoLibrary() {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            UIAlertController.showAlert("Image Picker", message: "Can't open photo library", target: self, completion: nil)
            return
        }
        
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.delegate = self
        
        self.present(self.imagePicker, animated: true)
    }
    
    func setData () {
       
        
    }
}

extension ImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        defer {
            picker.dismiss(animated: true)
        }
        
        // get the image
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        // At this time we have to convert the data
        let imageMBSize = image.getImageFileMBSize()
        if imageMBSize > 2 { // Cehck if the image size is greater that 2 MB
            let img2MB = image.compressTo(1)
            self.imgViewer.image = img2MB
        } else {
            // do something with it
            self.imgViewer.image = image
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        defer {
            picker.dismiss(animated: true)
        }
    }
}

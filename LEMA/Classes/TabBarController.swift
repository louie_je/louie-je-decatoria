//
//  TabBarController.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 30/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    let tabs = [Profile(), Events()]
    var user = EUsers()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTabViewControllers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Setup Tabs
extension TabBarController {
    func setupTabViewControllers () {
        
        let profileTabClass = Profile()
        profileTabClass.user = self.user
        
        let ProfileNavController = UINavigationController()
        ProfileNavController.viewControllers = [profileTabClass]
        ProfileNavController.tabBarItem = UITabBarItem(title: "Profile", image: #imageLiteral(resourceName: "ProfileTab"), tag: 1)
        
        let eventsTabClass = Events()
        eventsTabClass.user = self.user
        let EventsNavController = UINavigationController()
        EventsNavController.viewControllers = [eventsTabClass]
        EventsNavController.tabBarItem = UITabBarItem(title: "Events", image: #imageLiteral(resourceName: "EventsTab"), tag: 1)
        
        self.viewControllers = [ProfileNavController, EventsNavController]
        self.selectedIndex = 1
    }
}

//
//  BaseViewController.swift
//  EventsMgnt
//
//  Created by Louie Je Decatoria on 25/08/2017.
//
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    var isActive = true
    var keyBoardObserver = false
    
    deinit {
        // perform the deinitialization
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar()
        self.setNeedsStatusBarAppearanceUpdate()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.didRecognizeTap(_:)))
        tapGesture.cancelsTouchesInView = false;
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isActive = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.isActive = false
    }
    
    override func viewDidLayoutSubviews() { // called after viewDidLoad
        super.viewDidLayoutSubviews()
    }
    
    func configureNavBar() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func popNavigation() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func didRecognizeTap(_ tapGesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.keyBoardObserver = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}

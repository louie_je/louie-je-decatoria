//
//  UIView+Ext.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 30/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.addSubview(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.addSubview(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width:CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width + 100, height: width)
        self.addSubview(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.addSubview(border)
    }
    
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
}

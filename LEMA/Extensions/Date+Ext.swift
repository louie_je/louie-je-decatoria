//
//  Date+Ext.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 29/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import Foundation

extension Date {
    
    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    func getMonthAndDayName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd"
        let strMonthDay = dateFormatter.string(from: self)
        return strMonthDay
    }
    
    func getFullDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let strMonthDayYear = dateFormatter.string(from: self)
        return strMonthDayYear
    }
}

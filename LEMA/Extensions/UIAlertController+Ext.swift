//
//  UIAlertController+Ext.swift
//  LEMA
//
//  Created by Louie Je Decatoria on 30/08/2017.
//  Copyright © 2017 Louie Je Decatoria. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    class func showAlert(_ title: String?, message: String?, target: UIViewController?, completion: (()->Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in
            DispatchQueue.main.async {
                completion?()
            }
        })
        alert.addAction(defaultAction)
        target?.present(alert, animated: true, completion: nil)
    }
    
    // Invalid user credential
    class func showInvalidUserMsg(target: UIViewController?) {
        UIAlertController.showAlert("Login", message: "We cannot find the username or incorrect credentials.", target: target, completion: nil)
    }
}

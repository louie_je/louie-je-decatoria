//
//  UIButton+Ext.swift
//  EventsMgnt
//
//  Created by Louie Je Decatoria on 25/08/2017.
//
//

import Foundation
import UIKit

extension UIButton
{
    func setBackgroundColor(_ color: UIColor, forState: UIControlState)
    {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()?.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
}
